#!/bin/bash

#
#	POR   : Leonardo Bravo
#	WEB   : http://www.leobravo.cl
#	FECHA : 26/04/2015
#

#declare -ARRAY_RED vble_array
clear
echo "##################################"
echo ""
echo " RED SACAN 2.1"
echo ""
echo "##################################"

echo "Select an option:"
echo "1)search for printers"
echo "2)Search Host"
echo "3)Search VTR Acces Pont [Plugin]"
echo ">"
read opt

if [ $opt = "1" ]; then
	chmod +x printers.sh
	sudo ./printers.sh
fi

if [ $opt = "2" ]; then
	chmod +x host.sh
	sudo ./host.sh
fi

if [ $opt = "3" ]; then
	cd plugin/vtr/
	chmod +x vtr.sh
	./vtr.sh
fi
