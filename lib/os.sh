#!/bin/bash

#func para sacar un substring
substr() {
  aux=$(echo "$1" | awk '{split($0,a,"'$2'"); print a[2]}')
  echo "$aux" | awk '{split($0,a,"'$3'"); print a[1]}'
  rm -f .out
}

ip=$1
touch os.txt
nmap -O $ip | grep  OS > os.txt
os="Unknown"

  if `cat os.txt | grep linux-gnu 1>/dev/null 2<&1`
  then
  	os="GNU Linux"
  fi
  if `cat os.txt | grep Windows 1>/dev/null 2<&1`
  then
 	os="Microsoft Windows"
  fi
  if `cat os.txt | grep router 1>/dev/null 2<&1`
  then
	os="OpenWRT or DD-WRT [Router]"
  fi
  if `cat os.txt | grep Samsung 1>/dev/null 2<&1`
  then
	os="Android"
  fi

echo $os
rm -f os.txt

