#!/bin/bash

#
#	POR   : Leonardo Bravo
#	WEB   : http://www.leobravo.cl
#	FECHA : 22/02/2013
#

#declare -ARRAY_RED vble_array

substr() {
  aux=$(echo "$1" | awk '{split($0,a,"'$2'"); print a[2]}')
  echo "$aux" | awk '{split($0,a,"'$3'"); print a[1]}'
  rm -f .out
}

scanprinters(){
  root=1;
  if [ "$(whoami)" != 'root' ]; then
    echo "--->Starting network scans"
    sleep 2
    echo "--->ALERT!  You have no permission to run $0 as non-root user."
    root=0;
    sleep 3
    exit
  fi


  if [ $root = 0 ]; then
    echo "----->The system will make a basic scan of the network";
    sleep 2
    nmap -p 515,631,9100,9101,9102 $1/24  -oX printers.xml
    echo "--->scan completed";
    clear
  fi

  if [ $root = 1 ]; then
    #echo "----->The system will make a basic scan of the network";
    sleep 2
    nmap -sS -O -p 515,631,9100 $1/24  -oX printers.xml
    echo "--->scan completed";
    clear
  fi

  touch printers.dat

  while read line
  do
    echo "$line" >> printers.dat
  done < printers.xml

  rm -f printers.xml
}

# getname $ip $port
getname(){
  ip=$( echo $1 | sed 's/"//g' )
  port=$( echo $2 | sed 's/"//g' )

  if [ "$port" == "631" ]; then
	cups=$( curl -s http://$ip:$port/printers/ | grep "<TR><TD><A HREF=\"/printers/" | sed 's/A/@/g' )
	line=$(substr "$cups" "<@" "@>")
	name_printer=$( echo $line | sed 's/HREF//g' | sed 's/\///g' | sed 's/"//g' | sed 's/<//g' | sed 's/>//g' | sed 's/=//g' | sed 's/printers//g' )
	echo -e $name_printer
  fi

  #OTHER NO CUPS POR 631
  if [ "$port" == "631" ]; then
	web=$( curl -s http://$ip/web/guest/es/websys/webArch/topPage.cgi | grep "RICOH" | sed 's/ //g' | sed 's/<//g' | sed 's/td//g' | sed 's/nowrap//g' | sed 's/>//g')
        name_printer=$web
        echo -e $name_printer
  fi


  if [ "$port" == "9101" ] || [ "$port" == "9100" ] || [ "$port" == "9102" ] || [ "$port" == "9103" ]; then
	#Kyocera
        web_uno=$( curl -s http://$ip/ | grep "ModelName" | sed 's/var//g' | sed 's/"//g' | sed 's/=//g' | sed 's/ //g' )
	if [ -n "$web_uno" ]; then
		name_printer=$( echo "Kyocera " $web_uno )
	fi
	#RICOH
	web_dos=$( curl -s http://$ip/web/guest/es/websys/webArch/topPage.cgi | grep "RICOH" | sed 's/ //g' | sed 's/<//g' | sed 's/td//g' | sed 's/nowrap//g' | sed 's/>//g')
        if [ -n "$web_dos" ]; then
                name_printer=$web_dos
        fi
        echo -e $name_printer
  fi


}

showprinters(){

  cat printers.dat | sed 's/ //g' > printers_sin.txt
  rm -f printers.dat

  echo ""
  echo "##################################"
  echo "Printers found"
  #echo "Red: $red"
  echo "##################################"
  echo ""

  while read line
  do
    eval=0;
    #to search
    #address="addressaddr="
    address="addrtype=\"ipv4\""
    end_host="</host>"
    port="open"

    #VE EL HOST
    if `echo ${line} | grep "${end_host}" 1>/dev/null 2>&1`
    then
       cont=$(expr $cont + 1)
    fi

    #CAPTURA LA IP
    if `echo ${line} | grep "${address}" 1>/dev/null 2>&1`
    then
      ip=$(substr $line "addr=" "add")
      #ip=$(substr $line "addr=" "addrtype=\"ipv4\"")
    fi

    if `echo ${line} | grep "${port}" 1>/dev/null 2>&1`
    then
      echo -e "\nPrinter Nª"$cont
      echo "Host :" $ip
      echo "Port :" $(substr $line "portid=" ">")
      port=$(substr $line "portid=" ">")
      echo "Name Printer:" $(getname "$ip" "$port")
    fi

  done < printers_sin.txt
  rm -f printers_sin.txt

}

echo "Want to enter an IP manually? [s/n]"
read opt

if [ $opt = "s" ]; then
  echo "Enter the IP :"
  read red
  echo "La IP es:" $red
fi

if [ $opt = "n" ]; then
  ip_addr=$(ip addr show | grep 24)
  red=$(substr "$ip_addr"  "inet"  "/24")
  echo "--->Detecting ip of the machine"
  sleep 2
  echo "your ip is: $red"
fi

echo "want to find printers on other subnetworks? [s/n]"
read opt_dos

if [ $opt_dos = "n" ]; then
  scanprinters $red
  showprinters
  exit 1
fi


if [ $opt_dos = "s" ]; then
  N=0
  echo "red :"$red
  dos=$(substr "$red" "." ".")
  uno=$(substr "@$red" "@" ".")

  echo "--->Reachable networks:"

  for i  in $uno.$dos.{0..254}.1
  do
      if ping -c1 -w1 $i  &>/dev/null
      then
	      echo $i is up
	      ARRAYRED[$N]=$i
	      let N=N+1
      fi
  done

  echo "---"
  echo "reachable networks"
  echo "---"


  for ((i=0; i<${#ARRAYRED[@]}; ++i));
  do
      echo "red $i: ${ARRAYRED[$i]}";
      scanprinters ${ARRAYRED[$i]}
  done

showprinters
fi
