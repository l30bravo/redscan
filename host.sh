#!/bin/bash

#func para sacar un substring
substr() {
  aux=$(echo "$1" | awk '{split($0,a,"'$2'"); print a[2]}')
  echo "$aux" | awk '{split($0,a,"'$3'"); print a[1]}'
  rm -f .out
}

os() {
  ip=$1
  #touch os.txt
  #nmap -O $ip | grep  OS > os.txt
  os="Unknown"

  if `cat host.txt | grep "OS details" | grep linux-gnu 1>/dev/null 2<&1`
  then
  	os="GNU Linux"
  fi
  if `cat host.txt | grep "OS details" | grep Linux 1>/dev/null 2<&1`
  then
  	os="GNU Linux"
  fi
  if `cat host.txt | grep "OS details" | grep Windows 1>/dev/null 2<&1`
  then
 	os="Microsoft Windows"
  fi
  if `cat host.txt | grep "OS details" | grep router 1>/dev/null 2<&1`
  then
	os="OpenWRT or DD-WRT [Router]"
  fi
  if `cat host.txt | grep "OS details" | grep Android 1>/dev/null 2<&1`
  then
	os="Android"
  fi

echo $os
}

root=0;

###MAIN###
if [ "$(whoami)" = 'root' ]; then
    echo "--->Starting network scans"
    root=1;
fi


if [ $root = 0 ]; then
    echo "--->ALERT!  You have no permission to run $0 as non-root user."
   exit
fi

ip_addr=$(ip addr show | grep 24)

red=$(substr "$ip_addr"  "inet"  "/24")
uno=$(echo $red | awk -F'.' {'print $1'})
dos=$(echo $red | awk -F'.' {'print $2'})
tres=$(echo $red | awk -F'.' {'print $3'})

mac_addr=" "
echo "Escaneando ..."
for i in $uno.$dos.$tres.{0..254}
do
    #echo "Viendo "$uno"."$dos"."$tres"."
    if ping -c1 -w1 $i &>/dev/null
    then
	sudo nmap -O $i > host.txt
        #mac_addr=$(nmap -sP -n $i | grep MAC)
	mac_addr=$(cat host.txt | grep MAC | awk {'print $3'})
        echo $i "-" $mac_addr "-" $(os)
	rm -rf host.txt
    fi
done
