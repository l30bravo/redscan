#!/bin/bash

#
# VTR -Chile
# Modems
# 29/08/2013
#

#SACAR SUBSTRING
substr() {
	aux=$(echo "$1" | awk '{split($0,a,"'$2'"); print a[2]}')
	echo "$aux" | awk '{split($0,a,"'$3'"); print a[1]}'
}



echo "#IP MODEMS">vtr_ips.txt

cont=0

#OBTENIENDO LOS MODEMS DE VTR
while read line
do
	new_line=$(echo $line | sed 's/\// /g')
	red=$(echo $new_line | awk '{print $1}')
	mascara=$(echo $new_line  | awk '{print $2}')

	if [ $mascara = 15 ];
	then
		dos=$(substr "$new_line" "." ".")
		uno=$(substr "@$new_line" "@" ".")
		IP=$(echo $uno.$dos.{0..254}.{0..254})
		MAX=$((253*253*3))

	fi

        if [ $mascara = 16 ];
        then
                dos=$(substr "$new_line" "." ".")
                uno=$(substr "@$new_line" "@" ".")
                IP=$(echo $uno.$dos.{0..254}.{0..254})
                MAX=$((253*253*3))

        fi



	#RECORRE UN SEGMENTO DE LAS IPS DE VTR
	for i in $IP
	do
		clear
		echo -e "\n VTR Chile - Modems \n"
		echo $line

		bash progreso.sh $cont $MAX "Get Modems :\n"

		#SI EL MODEM ESTA RESPONDIENDO

		if ping -c1 -w1 $i  &>/dev/null
		then
			echo -e "\n ON : $i"
			#VER SI TIENE EL PUERTO 80 DISP
			ip_valida=`nmap -v --open -p 80 $i -oG - | grep Ports | awk '{print $2}' | sed 's/Ports//g' | sed 's/ //g'`

			if [ -n "$ip_valida" ];
			then
				ssid=`curl --silent "http://$i/" | grep height | tail -11 | head -1 | awk -F\> '{print $2}' | awk -F\< '{print $1}'`
				bssid=`curl --silent "http://$i/" | grep height | head -14 | tail -1 | awk -F\> '{print $2}' | awk -F\< '{print $1}'`
				wigle=$(python  wigle_v3.py  $ssid $bssid)
				echo -e "$ip_valida $ssid $bssid \n $wigle" >> vtr_ips.txt
				echo "------" >> vtr_ips.txt
				sleep 5 #EVITAR SER DETECTADO COMO BOOT POR WIGLE

			fi
		fi
	let cont=cont+1
	done
done < vtr_list_dinamic_ip.txt

#RESPALDO DE LOS MODEMS ENCONTRADOS
cp vtr_list_dinamic_ip.txt >> log.txt

echo -e "\n DONE \n" >> vtr_ips.txt



